FROM python:3.7.3-alpine

RUN pip install -U pipenv
RUN mkdir -p /app
WORKDIR /app
COPY Pipfile /app/Pipfile
COPY Pipfile.lock /app/Pipfile.lock
RUN pipenv install
COPY scripts /app/scripts
CMD ["pipenv", "run", "python", "-u", "scripts/main.py"]
