# KTC-Rate-Crawler

## Environment Setup

In order to use a script, you need to install `python3` first.
And then install the `pipenv` using

```bash
$ pip install pipenv
```

After that install dependencies of the project by

```bash
$ pipenv install
```

Finally, run all components including influxdb, chronograf and kapacitor by

```bash
$ docker-compose up -d
```

### Run script

```bash
$ docker-compose up --build
```

### Production build

I'm using container to store dependencies and ship app.

```bash
$ docker-compose build
```

## Todos

- [ ] data in some period is missing because we're sleep crawler script for 60 seconds but is's not actually 60 seconds, it's (60s + inserting data to db time)
