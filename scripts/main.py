import copy
import json
import requests
from influxdb import InfluxDBClient
from datetime import datetime, timedelta
from time import sleep

API_URL = 'https://www.exch.ktb.co.th/exchangerate/GetTravelCardRate'
CONFIG = {
    "db": {
        "host": "influxdb",
        "port": "8086",
        "database": "ktc_db"
    }
}

def gather_data():
    """Gather data from api and return json text or None if error occurs"""
    response = None
    try:
        print(' [*] %s Requesting url ... ' % datetime.now(), end='')
        response = requests.get(API_URL)
        print('done!')
        return response.text
    except:
        return response


def parse_data(data):
    """Parse data come from influxdb and return as dict"""
    arr = [] # keep parsed data
    data = json.loads(data) # convert response to dict

    # converting thai time from api to utc timezone by -7 hours
    server_date = datetime.strptime(data['sysDateTime'], '%d/%m/%Y %H:%M')
    server_date = server_date - timedelta(hours=7)

    template = {
        "measurement": "",
        "tags": {
            "host": "my-instance"
        },
        "time": server_date.strftime('%Y-%m-%dT%H:%M:%SZ'),
        "fields": {
            # res['crcd']: res['buyingRate'] # ex. JPY: 0.29
        }
    }

    for res in data['travelCardRateResp']['inquiryMCCExchangeRateRespRec']:
        # Buying rate
        buying_rate_json = copy.deepcopy(template)
        buying_rate_json['measurement'] = 'buying_rate'
        buying_rate_json['fields'][res['crcd']] = float(res['buyingRate']) # convert to int for avoid 'unsupported mean iterator type: *query.stringInterruptIterator'

        # Buying mcp rate
        buying_mcp_rate_json = copy.deepcopy(template)
        buying_mcp_rate_json['measurement'] = 'buying_mcp_rate'
        buying_mcp_rate_json['fields'][res['crcd']] = float(res['buyingMcpRate'])

        # Selling rate
        selling_rate_json = copy.deepcopy(template)
        selling_rate_json['measurement'] = 'selling_rate'
        selling_rate_json['fields'][res['crcd']] = float(res['sellingRate'])

        # Selling mcp rate
        selling_mcp_rate_json = copy.deepcopy(template)
        selling_mcp_rate_json['measurement'] = 'selling_mcp_rate'
        selling_mcp_rate_json['fields'][res['crcd']] = float(res['sellingMcpRate'])

        arr.append(buying_rate_json)
        arr.append(buying_mcp_rate_json,)
        arr.append(selling_rate_json)
        arr.append(selling_mcp_rate_json)
    
    return arr



def insert_to_influx(data):
    """Insert response data to influxdb"""
    try:
        try:
            client = InfluxDBClient(host=CONFIG['db']['host'], port=CONFIG['db']['port'], database=CONFIG['db']['database'])
            if not CONFIG['db']['database'] in client.get_list_database():
                print(' [*] %s Database not found, creating new database ... ' % datetime.now(), end='')
                client.create_database(CONFIG['db']['database'])
                print('done!')
            print(' [*] %s inserting data to influxdb ... ' % datetime.now(), end='')
            client.write_points(data)
            print('done!')
        except requests.exceptions.ConnectionError as ex:
            print(' [*] %s Connection error %s' % (datetime.now(), ex))
            pass
    finally:
        client.close()  
        pass



if __name__ == "__main__":
    while True:
        try:
            response = gather_data()
            if response is None:
                # if error occurs keep doing until we have a response
                continue
            parsed_data = parse_data(response)
            insert_to_influx(parsed_data)
            sleep(60)
        except:
            pass
